VALID_VALUES = {
    :byr => 1920..2002,
    :iyr => 2010..2020,
    :eyr => 2020..2030,
    :hgt_in => 59..76,
    :hgt_cm => 150..193,
    :ecl => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
}

def import_data(file)
    data = []
    s = IO.read(file).split("\n\n")
    s.each do |line|
        line.gsub!("\n", " ")
        item = Hash[line.scan(/(\w+):(\S+)/).map { |(first, second)| [first.downcase.to_sym, second] }]
        data.append(item)
    end
    return data
end

def contains_required_fields?(passport)
    required_fields = [:byr, :iyr, :eyr, :hgt, :hcl, :ecl, :pid]
    required_fields.each do |field|
        return false unless passport.has_key?(field)
    end
    return true
end

def valid_numeric_field?(passport, field)
    i = passport[field].to_i
    return true if VALID_VALUES[field].member?(i)
    return false
end

def valid_hgt?(passport)
    hgt = passport[:hgt].split(/(?=\D.)/)
    if hgt[1] == "cm"
        return true if VALID_VALUES[:hgt_cm].member?(hgt[0].to_i)
    elsif hgt[1] == "in"
        return true if VALID_VALUES[:hgt_in].member?(hgt[0].to_i)
    else
        return false
    end
    return false
end

def valid_hcl?(passport)
    hcl = passport[:hcl].split("",2)
    return false unless hcl[0] == "#"
    return true if hcl[1].length == 6 && hcl[1].match?(/([0-9]|[a-f])/)
    return false
end

def valid_ecl?(passport)
    ecl = passport[:ecl]
    return true if VALID_VALUES[:ecl].include?(ecl)
    return false
end

def valid_pid?(passport)
    return true if passport[:pid].length == 9
    return false
end

def count_valid_passports(passports)
    i = 0
    passports.each do |passport|
        next unless contains_required_fields?(passport) 
        if valid_pid?(passport) &&
            valid_ecl?(passport) &&
            valid_hcl?(passport) &&
            valid_hgt?(passport) &&
            valid_numeric_field?(passport, :byr) &&
            valid_numeric_field?(passport, :iyr) &&
            valid_numeric_field?(passport, :eyr)
            i += 1
        end
    end
    return i
end

f = File.open("input.txt")
passports = import_data(f)

puts count_valid_passports(passports)
