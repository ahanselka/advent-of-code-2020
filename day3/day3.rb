def get_trees(ary, r_step, d_step)
    puts "Running with right #{r_step} and down #{d_step}"
    c = 0
    range = 0..ary.length-1
    range.step(d_step) do |idx|
        loc = (idx * r_step) % (ary[idx].length)
        puts "loc is #{loc}"
        c += 1 if ary[idx][loc] == "#"
    end
    return c
end

def get_trees_2(ary, r_step, d_step)
    puts "Running with right #{r_step} and down #{d_step}"
    x = 0
    y = 0
    count = 0
    range = (0..ary.length-1)
    range.each do |idx|
        x = (x + r_step) % (ary[idx].length)
        y = y + d_step
        next if y > (ary.length-1)
        count += 1 if ary[y][x] == "#"
    end
    return count
end

f = File.open("input.txt")
m = IO.readlines(f, chomp: true)

part1 = get_trees(m, 3, 1)
puts "The answer to part 1 is: #{part1}"

part2_1 = get_trees_2(m, 1, 1)
part2_2 = get_trees_2(m, 3, 1)
part2_3 = get_trees_2(m, 5, 1)
part2_4 = get_trees_2(m, 7, 1)
part2_5 = get_trees_2(m, 1, 2)

part2 = part2_1 * part2_2 * part2_3 * part2_4 * part2_5

puts "The answers to the part 2 components are: #{part2_1}, #{part2_2}, #{part2_3}, #{part2_4}, #{part2_5}."
puts "The answer to part 2 is: #{part2}"
