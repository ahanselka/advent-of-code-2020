def get_trees(ary, r_step, d_step):
    print("Running with right %s and down %s"%(r_step, d_step))
    x, y, count = 0, 0, 0
    for i in range(0, len(ary)):
        y = y + d_step
        if y > (len(ary)-1):
            continue
        x = (x + r_step) % (len(ary[y]))
        if ary[y][x] == "#":
            count += 1
    return count

with open("input.txt") as file:
    ary = file.read().splitlines() 

part1 = get_trees(ary, 3, 1)
print("The answer to part 1 is: %s"%(part1))

part2_1 = get_trees(ary, 1, 1)
part2_2 = get_trees(ary, 3, 1)
part2_3 = get_trees(ary, 5, 1)
part2_4 = get_trees(ary, 7, 1)
part2_5 = get_trees(ary, 1, 2)

part2 = part2_1 * part2_2 * part2_3 * part2_4 * part2_5

print("The components of part2 are %s, %s, %s, %s, %s"%(part2_1,part2_2,part2_3,part2_4,part2_5))


print("The answer to part 1 is: %s"%(part2))
