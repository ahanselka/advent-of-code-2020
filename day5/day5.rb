def import_data(file)
    data = []
    s = IO.read(file).split("\n")
    s.each do |line|
        data.append(line)
    end
    return data
end

def find_seat(row)
    row = row.split("")
    r = []
    row.each do |i|
        case i
        when "F", "L"
            r.append(0)
        when "B", "R"
            r.append(1)
        else
            return "fail"
        end
    end
    row_number = r[0..6].join.to_i(2)
    seat_number = r[7..9].join.to_i(2)
    answer = [row_number, seat_number]
    return answer
end 

def find_seat_id(seat)
    id = (seat[0] * 8) + seat[1]
    return id
end

f = File.open("input.txt")
rows = import_data(f)
seats = []

rows.each do |row|
    seats.append(find_seat(row))
end

seat_ids = []
seats.each do |seat|
    seat_ids.append(find_seat_id(seat))
end

puts "The highest seat ID is #{seat_ids.max}"

(seat_ids.min..seat_ids.max).each do |r|
    if !seat_ids.include?(r)
        puts "Seat ID #{r} is missing. This is probably your seat!"
    end
end
