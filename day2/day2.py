import re

def organize_data(file):
    organized_data = []
    for line in f:
        data = {}
        result = re.split("[-:\s]", line)
        result = list(filter(None, result))
        data["min"] = result[0]
        data["max"] = result[1]
        data["letter"] = result[2]
        data["password"] = result[3]
        organized_data.append(data)
    return organized_data

def check_valid_passwords(data):
    i = 0
    for item in data:
        min = int(item["min"])
        max = int(item["max"])
        letter_count = item["password"].count(item["letter"])
        if letter_count >= min and letter_count <= max:
            i += 1
    return i

def check_valid_positions(data):
    i = 0
    for item in data:
        position1 = int(item["min"]) - 1
        position2 = int(item["max"]) - 1
        password = item["password"]
        letter = item["letter"]
        if (password[position1] == letter) ^ (password[position2] == letter):
            i += 1
    return i

f = open("input.txt", "r")
r = organize_data(f)

print(check_valid_passwords(r))
print(check_valid_positions(r))
