def organize_data(file)
    organized_data = []
    file.each do |line|
        data = {}
        result = line.split(%r{[-:\s]}).delete_if(&:empty?)
        data["min"] = result[0]
        data["max"] = result[1]
        data["letter"] = result[2]
        data["password"] = result[3]
        organized_data.append(data)
    end
    return organized_data
end

def check_valid_passwords(data)
    i = 0
    data.each do |item|
        min = item["min"].to_i
        max = item["max"].to_i
        letter_count = item["password"].count(item["letter"])
        i += 1 if letter_count >= min and letter_count <= max
    end
    return i
end

def check_valid_positions(data)
    i = 0
    data.each do |item|
        position1 = item["min"].to_i - 1
        position2 = item["max"].to_i - 1
        password = item["password"]
        letter = item["letter"]
        if (password[position1] == letter) ^ (password[position2] == letter)
            i += 1
        end
    end
    return i
end

f = File.open("input.txt")

r = organize_data(f)

puts "Valid passwords: #{check_valid_passwords(r)}"
puts "Valid positions: #{check_valid_positions(r)}"
