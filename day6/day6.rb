LETTERS = ("a".."z")

def import_data(file)
    data = []
    s = IO.read(file).split("\n\n")
    s.each do |line|
        fam = line.split("\n")
        data.append(fam)
    end
    return data
end

def count_unique(str)
    count = []
    str.split("").each do |i|
        count.append(i) unless count.include?(i)
    end
    a = count.length
end

def count_everyone(ary)
    count = 0
    ary.each do |family|
        LETTERS.each do |l|
            if family.all? { |w| w.include?(l) }
                count += 1
            end
        end
    end
    return count
end


f = File.open("input.txt")
answers = import_data(f)
n = []
answers.each do |family|     
    num = count_unique(family.join)
    n.append(num)
end

count_all = count_everyone(answers)

puts "The sum of unique yeses is #{n.inject(0, &:+)}"
puts "The sum of when everyone answered yes is #{count_all}"
