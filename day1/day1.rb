def two_sum(numbers, sum)
    numbers.each do |number|
        difference = sum - number
        if numbers.include?(difference)
            return [number, difference]
        end
    end
end

def three_sum(numbers, sum)
    (0..numbers.length - 3).each do |i|
        front = numbers[i]
        start_index = i + 1
        back_index = numbers.length - 1
        
        while start_index < back_index do 
            start = numbers[start_index]
            back = numbers[back_index]

            if front + start + back > sum
                back_index -= 1
            else
                start_index += 1
            end

            return [front, start, back] if front + start + back == sum
        end
    end
    false
end

                
                
input = File.open("./input.txt")

numbers = input.readlines.map(&:to_i)
numbers.sort!

two_sum = two_sum(numbers, 2020)

two_sum.inject(:*)

three_sum = three_sum(numbers, 2020)

three_sum.inject(:*)
