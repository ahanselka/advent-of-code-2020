def multiply(numbers):  
    total = 1
    for x in numbers:
        total *= x  
    return total  

def two_sum(numbers, sum):
    for i in numbers:
        difference = sum - i
        if difference in numbers:
            return [difference, i]


def three_sum(numbers, sum):
    for i in range(len(numbers) - 3):
        front = numbers[i]
        start_index = i + 1
        back_index = len(numbers) - 1
        while start_index < back_index:
            start = numbers[start_index]
            back = numbers[back_index]
            if front + start + back == sum:
                return [front, start, back]
            elif front + start + back > sum:
                back_index -= 1
            else:
                start_index += 1


f = open("input.txt", "r")
numbers = []
for line in f:
    numbers.append(int(line))

numbers.sort()

multiply(two_sum(numbers, 2020))

multiply(three_sum(numbers, 2020))
